Appunti da ordinare

da μεθοδος 🆗 @methodus

Nome|Autore|Risors
-|-|-
Guida al Fediverso|Devol|https://noblogo.org/devol/guida-alla-decentralizzazione-e-al-fediverso-con-i-devol
Guide||https://mastodon.uno/@methodus/109366329827076692
Tutorial brevi|@mastodonpertutti​@fediverse.blog|@mastodonpertutti​@fediverse.blog
Guida del bravo| @quinta |https://blog.quintarelli.it/2022/11/mastodon-come-alternativa-a-twitter-mastodon-non-e-twitter-e-antivirale-per-costruzione-la-sfida-principale-di-ogni-community/​
Apps:||https://mastodon.uno/@methodus/109361657553906827
Apps:||https://mastodon.uno/@methodus/109361668544089075
Cancellazione automatica e contenuti sensibili |@iusondemand​|https://mastodon.uno/@iusondemand/109313377123688527​
Cancellazione automatica e contenuti sensibili |@iusondemand​|https://mastodon.uno/@iusondemand/109313396064583847​
Supportare Mastodon:|methodus|https://mastodon.uno/@methodus/1093664
